import app from './app'
import colors from 'colors'
const PORT = process.env.PORT || "3000";

app.listen(PORT, () => {
    console.log((`SERVER LISTENING ON PORT : ${PORT}`).green);
    
});
