import mongoose from 'mongoose'
import CourseSchema from '../models/course'

export const addCourse = async (req, res) => {
    let newCourse = CourseSchema();
    newCourse.number = req.fields.number;
    newCourse.clues = req.fields.clues;
    newCourse.title = req.fields.title;
    newCourse.description = req.fields.description;

    return await newCourse.save((err, course) => {
        if (err) {
            res.send(err);
        }
        else {
            res.json(course);
            console.log("AJOUT D'UNE NOUVELLE COURSE :");
            console.log(JSON.stringify(course));
        }
    });
}

export const getCourses = async (req, res) => {
    await CourseSchema.find((err, courses) => {
        if (err) {
            res.send(err);
        }
        else {
            res.json(courses);
        }
    });
}

export const getCourse = async (req, res) => {
    await CourseSchema.findById(req.params.id, (err, course) => {
        if (err) {
            res.send(err);
        }
        else {
            res.json(course);
        }
    });
}

export const deleteCourse = async (req, res) => {
    await CourseSchema.findById(req.params.id).remove((err, course) => {
        if (err) {
            res.send(err);
        }
        else {
            res.json(course);
            console.log("SUPPRESSION D'UN PARCOURS : ");
            console.log(JSON.stringify(course))
        }
    });
}
