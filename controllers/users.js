import mongoose from 'mongoose'
import UserSchema from '../models/user'

import * as bcrypt from 'bcryptjs'
import { toUpper } from 'lodash'
import colors from 'colors'

const saltRounds = 10

export const addUser = async (req,res) => {
    let newUser = new UserSchema()
    newUser.lastname = toUpper(req.fields.lastname);
    newUser.firstname = req.fields.firstname;
    newUser.birthdate = req.fields.birthdate;
    newUser.password = bcrypt.hashSync(req.fields.password, saltRounds);
    newUser.email = req.fields.email;
        
    return await newUser.save((err, user) => {
        if (err) {
            res.send(err);
        }
        else {
            res.json(user);
            console.log(("AJOUT D'UN NOUVEL UTILISATEUR : " + JSON.stringify(user)).green)
        }
    });
}

export const getUsers = async (req, res) => {
    await UserSchema.find(function (err, users) {
        if(err) {
            res.send(err)
        }
        else {
            res.json(users)
        }
    });
}

export const getUser = async (req, res) => {
    await UserSchema.findById(req.params.id, function(err, user) {
        if(err) {
            res.send(err)
        }
        else {
            res.json(user)
        }
    });
}

export const deleteUser = async (req, res) => {
    await UserSchema.findById(req.params.id).remove(function(err,user) {
        if(err) {
            res.send(err)
        }
        else {
            res.json(user)
            console.log(("SUPPRESSION DE L'UTILISATEUR : " + req.params.id).red)
        }
    });
}