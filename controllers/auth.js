import * as bcrypt from 'bcryptjs'
import JWT from 'expo-jwt'
import UserSchema from '../models/user'

const SECRET = 'secret';

export const login = (req, res) => {
    UserSchema.findOne({email: req.fields.email})
        .then(function (user) {
            if(!user) {
                return res.status(404).json({
                    message: 'EMAIL_NOT_FOUND'
                });
            }
            else {
                bcrypt.compare(req.fields.password, user.password).then(function(resultat) {
                    if(resultat) {
                        return res.status(200).json({
                            success: 'JWT auth',
                            token: JWT.encode({email: user.email, _id: user._id}, SECRET)
                        })
                    }
                    else {
                        return res.status(404).json({
                            message: 'BAD_PASSWORD'
                        })
                    }
                });
            }
        }).catch((err) => {
            console.log(err)
        })
}
