import express from 'express'
import helmet from 'helmet'
import cors from 'cors'
import mongoose from 'mongoose'
const formidable = require('express-formidable')
import { Routes } from './routes/routes'

class App {
    constructor() {
        this.app = express();
        this.config();
        this.routing(this.app);
    }

    config() {
        // Helmet for security
        this.app.use(helmet());
        // Setup CORS
        this.app.use(cors())
        this.app.use(formidable())
        mongoose.Promise = global.Promise;
        mongoose.connect("mongodb+srv://TreasureHunt:lol@cluster0-bvcg9.mongodb.net/test?retryWrites=true&w=majority");
    }

    routing(app) {
        let routesList = new Routes();
        routesList.routes(app);
    }
}

export default new App().app;