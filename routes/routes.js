import * as userController from '../controllers/users'
import * as courseController from '../controllers/courses'
import { login } from '../controllers/auth'

const API_USERS = '/api/users';
const API_USERS_ID = '/api/users/:id';
const API_LOGIN = '/api/login';
const API_COURSES = '/api/courses';
const API_COURSES_ID = '/api/courses/:id';

export class Routes {

    routes(app) {
        app.route(API_USERS)
        // Ajout d'un utilisateur
            .post(userController.addUser)
        // Récupération de tous les utilisateurs
            .get(userController.getUsers)

        app.route(API_USERS_ID)
        // Récupération d'un utilisateur en fonction de son id
            .get(userController.getUser)
        // Suppression d'un utilisateur en fonction de son id
            .delete(userController.deleteUser)

        app.route(API_LOGIN)
        // Connexion de l'utilisateur, récupération d'un JWT
            .post(login)
        
        app.route(API_COURSES)
        // Ajout d'un parcours
            .post(courseController.addCourse)
        // Récupération de tous les parcours
            .get(courseController.getCourses)

        app.route(API_COURSES_ID)
        // Récupération d'un parcours en fonction de son id
            .get(courseController.getCourse)
        // Suppression d'un parcours en fonction de son id
            .delete(courseController.deleteCourse)
    }
}

