import mongoose from 'mongoose'

let CourseSchema = mongoose.Schema({
    title: {
        type: String
    },
    description: {
        type: String
    },
    clues: [{
        number: {
            type: Number
        },
        description: {
            type: String
        },
        coords: {
            latitude: {
                type: Number
            },
            longitude: {
                type: Number
            }
        }
    }]
});

export default mongoose.model('Course', CourseSchema);