import mongoose from 'mongoose'

let UserSchema = mongoose.Schema({
    lastname: {
        type: String,
    },
    firstname: {
        type: String,
    },
    birthdate: {
        type: String,
    },
    email: {
        type: String,
        unique: true
    },
    password: {
        type: String,
    }
});

export default mongoose.model('User', UserSchema);